class CardDeck {
	constructor() {

		this.arrCards = [];

		let suit = {
			D: 'diams',
			H: 'hearts',
			C: 'clubs',
			S: 'spades'
		};

		let rank = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'];


		let Coloda = () => {

			for ( let i = 0; i < rank.length; i++ ) {
				for ( let key in suit ) {
					let card = {
						rank: rank[i],
						suit: suit[key]
					};
					this.arrCards.push(card);
				}
			}
		};
		Coloda();
	};

	getCard() {
		let random = Math.floor(Math.random() * this.arrCards.length);
		return this.arrCards.splice(random, 1);
	};

	getCards(howMany) {
		let cards = [];
		for ( let i = 0; i < howMany; i++ ) {
			let card = this.getCard()[0];
			cards.push(card);
		}
		return cards;
	};
}

export default CardDeck;
