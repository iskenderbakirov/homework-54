

class PokerHand {
    constructor (arr) {
	    this.arr = arr;
    }

    getOutcome () {
        let result = '';

	    let rankResult = 0;
	    let suitResult = 0;
	    for ( let i = 0; i < this.arr.length; i++ ) {
		    for ( let j = i + 1; j < this.arr.length; j++ ) {
			    if ( this.arr[i].rank === this.arr[j].rank ) {
				    rankResult++;
			    }
			    if ( this.arr[i].suit === this.arr[j].suit ) {
				    suitResult++;
			    }
		    }
	    }

	    if ( rankResult === 0 ) {
		    result = 'Ничего!';
	    }
	    if ( rankResult === 1 ) {
		    result = 'Пара';
	    }
	    if ( rankResult === 2 ) {
		    result = '2 Пары';
	    }
	    if ( rankResult === 3 ) {
		    result = 'Тройка';
	    }
	    if ( rankResult === 4 ) {
		    result = 'Фулл Хаус';
	    }
	    if ( rankResult === 6 ) {
		    result = 'Карэ';
	    }
	    if ( suitResult === 10 ) {
		    result = 'Флеш';
	    }
	    return result;

    }

}

export default PokerHand;