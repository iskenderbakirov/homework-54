import React, {Component} from 'react';
import Card from './components/Card/Card';
import CardDeck from './libs/CardDeck';
import PokerHand from './libs/PokerHand';
import './App.css';

class App extends Component {
	state = {
		cards: []
	};

	addCard = () => {
		const cards = new CardDeck().getCards(5);
		this.setState({ cards });
	};

	pokerHand = () => {
		return new PokerHand(this.state.cards).getOutcome();
	};

	render() {
		return (
			<div className="App container">
				<Card
					addhandler={this.addCard}
					cards={this.state.cards}
				    hands={this.pokerHand}
				/>
				<h2> Комбинация: {this.pokerHand()}</h2>
			</div>
		);
	}
}

export default App;
